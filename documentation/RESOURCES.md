# Recursos utilizados no desenvolvimento
## Coleção de links que utilizei para fazer diferentes aspectos do desenvolvimento, em nenhuma ordem em particular

- [Artigo do GDQuest sobre FSMs (Finite State Machines) em Godot](https://www.gdquest.com/tutorial/godot/design-patterns/finite-state-machine/)

- [Vídeo tutorial do GDQeust sobre movimentação 3D em terceira pessoa na Godot 3](https://www.youtube.com/watch?v=UpF7wm0186Q)

- [Playlist de vídeos tutriais sobre como criar um character controller 3D em terceira pessoa na Godot 3](https://www.youtube.com/playlist?list=PLqbBeBobXe09NZez_1LLRcT7NQ9NfUCBC)

- [Vídeo do GDQuest sobre design patterns úteis para Godot](https://www.youtube.com/watch?v=oIrvZDDWxhU)

- [Mixamo: coleção de modelos 3D e animações para usar de placeholder](https://www.mixamo.com)

- [Character Animation Combiner: Ferramenta para combinar animações do mixamo em um único modelo 3D](https://nilooy.github.io/character-animation-combiner/)