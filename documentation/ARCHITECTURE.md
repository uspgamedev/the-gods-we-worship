# Arquitetura dos sistemas de The Gods We Worship

Esse arquivo tem a intenção de documentar o estado atual das ideias de arquitetura dos sistemas que estão sendo desenvolvidos em The Gods We Worship.

**IMPORTANTE**: Como o jogo está nas etapas iniciais do desenvolvimento, a arquitetura inteira está sujeita a mudanças drásticas. Nada que e está escrito aqui está bem definido.

## **Simulação (`Simulation`)**

Sistema responsável, em alto nível, por todo o jogo. A simulação é responsável por registrar e manter atualizadas as entidades, sendo a parte que junta todos os pedaços do sistema.

### **Entidade (`Entity`)**

Unidade mínima do sistema. Guarda apeans um ID, registrado pela simulação. Sua funcionalidade é dada por sua composição.

Entidades são compostas de 4 partes: os controladores, a máquina de estados, o resolvedor de efeitos e as consequências.

#### **Controladores (`Controller`)**

São uma espécie de mistura entre componentes e sistemas de uma arquitetura de ECS (Entity Component Systems) convencional. São responsáveis por guardar dados referentes à sua função e possuem funcionalidades de baixo nível. Essas funcionalidades devem ser desenvolvidas como uma forma de API que possa ser chamada pelos estados.

Um exemplo de um controlador seria um controlador de personagem físico. Esse controlador poderia ser representado pelo nó `CharacterBody3D` da Godot, que já possui toda a funcionalidade necessária. Essas funcionalidades seriam, então, invocadas pelos estados da entidade.

Não há uma classe padrão `Controller` da qual todos os controladores herdam pois eu pretendo usar alguns nós diferentes da Godot como controladores já prontos (vide exemplo que dei no parágrafo acima). Por conta do alto uso de herança na arquitetura da godot, eu não consigo criar uma classe padrão que atenda a todos os casos específicos, então preferi apenas não criar uma.

Um exemplo de conflito de herança seria, por exemplo, com um `PhysicsCharacterController`, que herdaria de `CharacterBody3D`, e um `AnimationController`, que herdaria de `AnimationPlayer`. Como as duas classes mães estão em galhos de herança diferentes, seria impossível fazer uma classe base `Controller` que atenda às necessidades das duas.

#### **Máquina de estados (`StateMachine`)**

A máquina de estados em si possui como única função administrar os estados da entidade. Cada máquina pode possuir inúmeros estados, mas apenas um deles pode estar ativo em um dado momento.

Os estados (`State`) em si são o que possuem funcionalidade. Eles leem os dados dos controladores da entidade e utilizam a funcionalidade dos próprios controladores para dar vida às entidades. Além disso, eles definem quais são as transições possíveis de um estado para outro.

Por exemplo, poderíamos ter o estado de `Idle` do jogador. Este estado é responsável por chamar o `AnimationController` de sua entidade e tocar a animação correspondente. Além disso, é responsável por realizar transições de estado caso algum input de movimento seja ativado. Ele pode transicionar para o estado `Walking` caso o jogador pressione o input de movimento no controle. Dessa forma, o novo estado ativo seria o `Walking`, que utilizaria o `PhysicsCharacterController` de sua entidade para realizar o movimento e o `AnimationController` para tocar a animação correspondente.

#### **Resolvedor de efeitos (`EffectResolver`)**

Assim como a máquina de estados, o resolvedor de efeitos tem como única função administrar os efeitos que são adicionados a ele, assim como as interações (`Interaction`) de cada um deles.

Efeitos podem ser adicionados a entidades por ações delas mesmas ou de outras entidades.

Os efeitos em si trazem apenas dados dentro deles, além de uma condição (`Condition`) e uma consquência (`Consequence`), caso essa condição seja atingida. Embora efeitos não tenham funcionalidades externas, eles ainda são responsáveis por atualizar a si mesmos.

É definido como uma interação (`Interaction`) um conjunto de condições e consequências de um efeito. Uma mesma interação pode ter mais de uma condição e mais de uma consequência, e um efeito pode ter mais de uma interação.

#### **Consequências (`Consequence`)**

Consequências são criadas pelo resolvedor de efeitos caso a condição desse efeito seja realizada. Toda a lógica de um evento está dentro da consequência, já que o efeito carrega apenas dados sobre si.

#### **Exemplo prático**

Um exemplo da interação entre todos esses sistemas pode ser a implementação de um sistema de debuffs.

Digamos que temos os debuffs de queimadura e de eletrocução. No entanto, esses debuffs não são ativados automaticamente. Eles devem ser acumulados na entidade até atingirem um certo nível, só então sendo ativados (de forma semelhante a como funcionam os efeitos de envenenamento, congelamento ou sangramento em Elden Ring e outros Souls).

Digamos também que, caso os efeitos de queimadura e eletrocução estejam ativados ao mesmo tempo, eles cuasam uma explosão na entidade (de forma semelhante ao que ocorre em Genshin Impact). Daremos o nome dessa explosão de sobrecarga.

Dessa forma, podemos definir os seguintes efeitos, interações, condições e consequências:

- `BurnBuildupEffet`: carrega apenas o campo `buildup`, que quantifica a acumulação de queimadura na entidade. Além disso, possui apenas uma `Interaction`:
    - `BurnBuildupInteraction`: carrega apenas uma `Condition` e uma `Consequence`:
        - `BurnBuildupCondition`: checa se o campo `buildup` do efeito atingiu um certo nível determinado pela condição.
        - `BurnBuldupConsequence`: adiciona o efeito `BurnEffect` na entidade.

- `BurnEffect`: carrega apenas o campo `amount`, que quantifica o quanto de queimadura a entidade ainda vai receber. Esse valor deve ser decrescido pelo efeito em sua atualização. Ele carrega duas `Interaction`s:
    - `OverloadInteraction`: se trata de uma `Interaction` exclusiva, o que significa que um mesmo `EffectResolver` só pode possuir uma instância dessa `Interaction`. Ela carrega apenas uma `Condition` e uma `Consequence`:
        - `OverloadCondition`: checa se a entidade possui os seguintes efeitos simultaneamente: `BurnEffect` e `ElectrocutionEffect`.
        - `OverloadConsequence`: modifica o estado atual da entidade para o estado `KnockedDown` e adiciona o efeito `OverloadVisualEffect` na entidade, responsável pelo efeito visual da sobrecarga. Esse novo efeito não carrega nenhuma interaction e remove a si mesmo depois de um tempo.
    - `RemoveBurnInteraction`: carrega apenas uma `Condition` e uma `Consequence`
        - `RemoveBurnCondition`: checa se o campo `amount` do efeito chegou a 0.
        - `RemoveBurnConsequence`: faz com que o efeito `BurnEffect` seja removido da entidade, caso exista.

De forma análoga, teríamos os efeitos `ElectrocutionBuildupEffect` e `ElectrocutuionEffect`, que possuiriam `Interaction`s, `Condition`s e `Consequences` semelhantes.

Dessa forma, pudemos modelar um sistema de debuffs em que os debuffs podem até mesmo interagir entre si de forma simples com `Effect`s, `Interaction`s, `Condition`s e `Consequences`.