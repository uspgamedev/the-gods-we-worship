class_name Entity
extends Node

const INVALID_ID: int = -1;

@onready var controllers_node: Node = $Controllers;
@onready var effect_resolver: EffectResolver = $EffectResolver;

var id: int = Entity.INVALID_ID;
var controllers: Array[Node];


func _ready() -> void:
	self.add_to_group(Groups.ENTITY);


func find_controller(type: Script) -> Node:
	for controller in controllers:
		if is_instance_of(controller, type):
			return controller;
	return null;


func find_controllers(types: Array[Script]) -> Array[Node]:
	var found: Array[Node] = [];
	for type in types:
		found.append(find_controller(type));
	return found;


func register_controller(controller: Node) -> void:
	controllers.append(controller);
