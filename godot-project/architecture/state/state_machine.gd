class_name StateMachine
extends Node

signal transitioned(previous_state, new_state);

@export var initial_state: State;

var state: State;

func _ready() -> void:
	self.add_to_group(Groups.STATE_MACHINE);
	
	await self.owner.ready;
	for child in self.get_children():
		var child_state := child as State;
		child_state.state_machine = self;
	state = initial_state;
	state.enter();


func _unhandled_input(event: InputEvent) -> void:
	state.handle_behavior_input(event);
	state.handle_input(event);


func _process(delta: float) -> void:
	state.update_behaviors(delta);
	state.update(delta);


func _physics_process(delta: float) -> void:
	state.physics_update_behaviors(delta);
	state.physics_update(delta);


func transition_to(target_state_name: String, msg: Dictionary = {}) -> void:
	if not has_node(target_state_name):
		return;
	
	state.exit();
	state = self.get_node(target_state_name);
	state.enter(msg);
	self.transitioned.emit(state.name);
