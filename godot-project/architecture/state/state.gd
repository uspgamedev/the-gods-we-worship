class_name State
extends Node

var state_machine: StateMachine;


func _ready() -> void:
	self.add_to_group(Groups.STATE);


func enter(_msg := {}) -> void:
	pass


func handle_input(_event: InputEvent) -> void:
	pass


func update(_delta: float) -> void:
	pass


func physics_update(_delta: float) -> void:
	pass


func exit() -> void:
	pass


func handle_behavior_input(event: InputEvent) -> void:
	for child in self.get_children():
		if child is Behavior and child.enabled:
			(child as Behavior).handle_input(event);


func physics_update_behaviors(delta: float) -> void:
	for child in self.get_children():
		if child is Behavior and child.enabled:
			(child as Behavior).physics_update(delta);


func update_behaviors(delta: float) -> void:
	for child in self.get_children():
		if child is Behavior and child.enabled:
			(child as Behavior).update(delta);
