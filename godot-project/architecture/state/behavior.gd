class_name Behavior
extends Node

@export var enabled: bool = true;


func _ready() -> void:
	self.add_to_group(Groups.BEHAVIOR);


func handle_input(_event: InputEvent) -> void:
	pass


func update(_delta: float) -> void:
	pass


func physics_update(_delta: float) -> void:
	pass
