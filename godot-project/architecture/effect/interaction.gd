class_name Interaction
extends Node

signal condition_met();

@export var exclusive: bool = false;

var entity: Entity;


func activate_consequences() -> void:
	for consequence in self.get_children():
		consequence = consequence as Consequence;
		consequence.activate();
