class_name EffectResolver
extends Node

signal effect_added(type: Script);
signal effect_removed(type: Script);
signal interaction_added(interaction: Interaction);
signal interaction_removed(interaction: Interaction);

@onready var effects: Node = $Effects;
@onready var interactions: Node = $Interactions;

var entity: Entity;


func add_effect(effect_scene: PackedScene) -> void:
	var effect := effect_scene.instantiate() as Effect;
	effect.entity = entity;
	effects.add_child(effect);
	effect_added.emit(effect.get_script());
	add_interactions(effect.interaction_scenes);


func remove_effect(type: Script) -> void:
	var effect := get_effect(type);
	if not effect: return;
	effects.remove_child(effect);
	effect_removed.emit(effect.get_script());


func has_effect(type: Script) -> bool:
	for effect in effects.get_children():
		if is_instance_of(effect, type):
			return true;
	return false;


func has_effects(types: Array[Script]) -> Array[bool]:
	var res: Array[bool] = [];
	for type in types:
		res.append(has_effect(type as Script));
	return res;


func get_effect(type: Script) -> Effect:
	for effect in effects.get_children():
		if is_instance_of(effect, type):
			return effect;
	return null;


func get_effects(types: Array[Script]) -> Array[Effect]:
	var res: Array[Effect] = [];
	for type in types:
		res.append(get_effect(type as Script));
	return res;


func get_all_effects() -> Array[Effect]:
	return effects.get_children() as Array[Effect];


func add_interaction(interaction_scene: PackedScene) -> void:
	var interaction := interaction_scene.instantiate() as Interaction;
	if interaction.exclusive and has_interaction(interaction.get_script()): 
		return;
	interaction.entity = entity;
	interactions.add_child(interaction);
	interaction_added.emit(interaction);


func add_interactions(interaction_scenes: Array[PackedScene]) -> void:
	for interaction_scene in interaction_scenes:
		add_interaction(interaction_scene);


func remove_interaction(interaction: Interaction) -> void:
	if interactions.is_ancestor_of(interaction):
		interactions.remove_child(interaction);
		interaction_removed.emit(interaction);


func has_interaction(type: Script) -> bool:
	for interaction in interactions.get_children():
		if is_instance_of(interaction, type):
			return true;
	return false;


func has_interactions(types: Array[Script]) -> Array[bool]:
	var res: Array[bool] = [];
	for type in types:
		res.append(has_interaction(type as Script));
	return res;


func get_interaction(type: Script) -> Interaction:
	for interaction in interactions.get_children():
		if is_instance_of(interaction, type):
			return interaction;
	return null;


func get_interactions(types: Array[Script]) -> Array[Interaction]:
	var res: Array[Interaction] = [];
	for type in types:
		res.append(get_interaction(type as Script));
	return res;


func get_all_interactions() -> Array[Interaction]:
	return interactions.get_children() as Array[Interaction];
