# meta-name: Controller
# meta-description: Default script to create new Controller scripts

class_name 
extends _BASE_


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
