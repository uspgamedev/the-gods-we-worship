# meta-name: Behavior
# meta-description: Default script to create new Behavior scripts

class_name 
extends Behavior


func handle_input(_event: InputEvent) -> void:
	pass


func update(_delta: float) -> void:
	pass


func physics_update(_delta: float) -> void:
	pass
