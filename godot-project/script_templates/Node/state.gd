# meta-name: State
# meta-description: Default script to create new State scripts

class_name
extends State


func enter(_msg := {}) -> void:
	pass


func update(_delta: float) -> void:
	pass


func physics_update(delta: float) -> void:
	pass


func exit() -> void:
	pass
