class_name Groups 
extends Node

const ENTITY: String = "Entity";
const CONTROLLER: String = "Controller";

const STATE_MACHINE: String = "State Machine";
const STATE: String = "State";
const BEHAVIOR: String = "Behavior";
