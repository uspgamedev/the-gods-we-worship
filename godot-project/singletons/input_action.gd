class_name InputAction
extends Node

# Player movement actions
const PLAYER_LEFT: String = "player_left";
const PLAYER_RIGHT: String = "player_right";
const PLAYER_FORWARD: String = "player_forward";
const PLAYER_BACKWARD: String = "player_backward";
const PLAYER_JUMP: String = "player_jump";
const PLAYER_RUN: String = "player_run";

# Player card actions
const PLAYER_CYCLE_CARDS_LEFT: String = "player_cycle_cards_left";
const PLAYER_CYCLE_CARDS_RIGHT: String = "player_cycle_cards_right";
const PLAYER_ACTIVATE_CARD_LEFT: String = "player_activate_card_left";
const PLAYER_ACTIVATE_CARD_RIGHT: String = "player_activate_card_right";

# Camera actions
const CAMERA_LEFT: String = "camera_left";
const CAMERA_RIGHT: String = "camera_right";
const CAMERA_FORWARD: String = "camera_forward";
const CAMERA_BACKWARD: String = "camera_backward";

