class_name Demo2FallBehavior
extends Behavior

@onready var physics_character_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(delta: float) -> void:
	physics_character_controller.velocity.y -= physics_character_controller.gravity * delta;
