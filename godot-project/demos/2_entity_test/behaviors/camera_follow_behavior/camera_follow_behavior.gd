class_name Demo2CameraFollowBehavior
extends Behavior

@export var camera_target: Node3D;
@export var follow_speed: float = 20;

@onready var camera_controller := (owner as Entity).find_controller(Demo2CameraController) as Demo2CameraController;


func physics_update(delta: float) -> void:
	camera_controller.global_position = lerp(camera_controller.global_position, camera_target.global_position, follow_speed * delta);
	camera_controller.global_rotation.x = lerp_angle(camera_controller.global_rotation.x, camera_target.global_rotation.x, follow_speed * delta);
	camera_controller.global_rotation.y = lerp_angle(camera_controller.global_rotation.y, camera_target.global_rotation.y, follow_speed * delta);
	camera_controller.global_rotation.z = lerp_angle(camera_controller.global_rotation.z, camera_target.global_rotation.z, follow_speed * delta);

