class_name Demo2RotateSpringArmBehavior
extends Behavior

@export var camera_speed: float = 100;

@onready var camera_controller := (owner as Entity).find_controller(Demo2CameraController) as Demo2CameraController;
@onready var spring_arm_controller := (owner as Entity).find_controller(Demo2SpringArmController) as Demo2SpringArmController;


func physics_update(delta: float) -> void:
	spring_arm_controller.rotation_degrees.x += camera_controller.pitch_input * camera_speed * delta;
	spring_arm_controller.rotation_degrees.x = clampf(spring_arm_controller.rotation_degrees.x, -50.0, 30.0);
	
	spring_arm_controller.rotation_degrees.y -= camera_controller.yaw_input *  camera_speed * delta;
	spring_arm_controller.rotation_degrees.y = wrapf(spring_arm_controller.rotation_degrees.y, 0, 360);

