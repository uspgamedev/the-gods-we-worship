class_name Demo2MoveBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(_delta: float) -> void:
	physics_controller.move_and_slide();
