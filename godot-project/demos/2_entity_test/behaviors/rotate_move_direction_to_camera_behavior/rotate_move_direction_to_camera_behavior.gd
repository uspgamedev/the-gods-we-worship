class_name Demo2RotateMoveDirectionToCameraBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;
@onready var spring_arm_controller := (owner as Entity).find_controller(Demo2SpringArmController) as Demo2SpringArmController;


func physics_update(_delta: float) -> void:
	physics_controller.move_direction = physics_controller.move_direction.rotated(Vector3.UP, spring_arm_controller.rotation.y);
