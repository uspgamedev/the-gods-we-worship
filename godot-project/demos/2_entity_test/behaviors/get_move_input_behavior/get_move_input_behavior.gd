class_name Demo2GetMoveInputBehavior
extends Behavior

@export var always_normalize: bool = false;

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(_delta: float) -> void:
	physics_controller.move_direction = Vector3(
		Input.get_axis(InputAction.PLAYER_LEFT, InputAction.PLAYER_RIGHT),
		0,
		Input.get_axis(InputAction.PLAYER_FORWARD, InputAction.PLAYER_BACKWARD)
	);
	
	if always_normalize or physics_controller.move_direction.length() > 1.0:
		physics_controller.move_direction = physics_controller.move_direction.normalized();
