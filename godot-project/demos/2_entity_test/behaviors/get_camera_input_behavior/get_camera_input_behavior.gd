class_name Demo2GetCameraInputBehavior
extends Behavior

@onready var camera_controller := (owner as Entity).find_controller(Demo2CameraController) as Demo2CameraController;


func physics_update(_delta: float) -> void:
	camera_controller.pitch_input = Input.get_axis(InputAction.CAMERA_BACKWARD, InputAction.CAMERA_FORWARD);
	camera_controller.yaw_input = Input.get_axis(InputAction.CAMERA_LEFT, InputAction.CAMERA_RIGHT);
