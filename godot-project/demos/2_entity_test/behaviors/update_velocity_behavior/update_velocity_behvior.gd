class_name Demo2UpdateVelocityBehavior
extends Behavior

@export var physics_controller_property_name: String;
@export var lerp_speed: float = 7;

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(delta: float) -> void:
	var new_speed = physics_controller.get(physics_controller_property_name);
	physics_controller.speed = lerp(physics_controller.speed, new_speed, lerp_speed * delta);
	physics_controller.velocity.x = physics_controller.move_direction.x * physics_controller.speed;
	physics_controller.velocity.z = physics_controller.move_direction.z * physics_controller.speed;
