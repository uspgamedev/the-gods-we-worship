class_name Demo2FaceMoveDirectionBehavior
extends Behavior

@onready var visual_controller := (owner as Entity).find_controller(Demo2VisualController) as Demo2VisualController;
@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(delta: float) -> void:
	if physics_controller.move_direction.length() > 0.1:
		physics_controller.look_direction = Vector2(
			physics_controller.velocity.z, 
			physics_controller.velocity.x
		);
		visual_controller.rotation.y = lerp_angle(
			visual_controller.rotation.y, 
			physics_controller.look_direction.angle(), 
			physics_controller.rotation_speed * delta
		);
