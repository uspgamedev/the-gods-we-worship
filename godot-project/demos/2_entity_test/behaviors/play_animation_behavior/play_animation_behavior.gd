class_name Demo2PlayAnimationBehavior
extends Behavior

@export var animation_name: StringName;
@export var custom_blend: float = 0.3;
@export var custom_speed: float = 1;

@onready var animation_controller := (owner as Entity).find_controller(Demo2AnimationController) as Demo2AnimationController;


func update(_delta: float) -> void:
	animation_controller.play(animation_name, custom_blend, custom_speed);
