class_name Demo2JumpBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func physics_update(_delta: float) -> void:
	if physics_controller.just_jumped:
		$"../MoveBehavior".enabled = false;
		await self.get_tree().create_timer(0.45).timeout;
		$"../MoveBehavior".enabled = true;
		physics_controller.velocity.y = physics_controller.jump_strength;
		physics_controller.floor_snap_length = 0;
		physics_controller.just_jumped = false;
	elif physics_controller.just_landed:
		physics_controller.floor_snap_length = 0.1;
	physics_controller.just_landed = physics_controller.is_on_floor() and physics_controller.floor_snap_length == 0;
