class_name Demo2PlayerMovementJumpingState
extends State

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func enter(_msg := {}) -> void:
	physics_controller.just_jumped = true;


func physics_update(_delta: float) -> void:
	if physics_controller.just_landed:
		state_machine.transition_to("Idle");


func exit() -> void:
	physics_controller.just_landed = false;
