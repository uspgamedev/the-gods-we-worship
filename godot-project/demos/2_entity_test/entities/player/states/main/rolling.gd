class_name Demo2PlayerMovementRollingState
extends State

@onready var animation_controller := (owner as Entity).find_controller(Demo2AnimationController) as Demo2AnimationController;


func enter(_msg := {}) -> void:
	animation_controller.animation_finished.connect(func(): state_machine.transition_to("Idle"));


func physics_update(_delta: float) -> void:
	pass


func exit() -> void:
	pass
