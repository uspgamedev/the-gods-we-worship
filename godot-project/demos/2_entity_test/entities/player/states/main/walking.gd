class_name Demo2PlayerMovementWalkingState
extends State

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func _ready() -> void:
	$RunButtonHoldTimer.timeout.connect(on_run_button_hold_timer_timeout);


func enter(_msg := {}) -> void:
	pass


func physics_update(_delta: float) -> void:
	if Input.is_action_pressed(InputAction.PLAYER_RUN) and $RunButtonHoldTimer.is_stopped():
		$RunButtonHoldTimer.start();
	elif Input.is_action_just_released(InputAction.PLAYER_RUN):
		state_machine.transition_to("Rolling");
		$RunButtonHoldTimer.stop();
	
	if physics_controller.move_direction.length() <= 0.1:
		state_machine.transition_to("Idle");
	
	if Input.is_action_just_pressed(InputAction.PLAYER_JUMP):
		state_machine.transition_to("Jumping");
	
	$PlayAnimationBehavior.custom_speed = physics_controller.move_direction.length();


func exit() -> void:
	$RunButtonHoldTimer.stop();


func on_run_button_hold_timer_timeout() -> void:
	state_machine.transition_to("Running");
