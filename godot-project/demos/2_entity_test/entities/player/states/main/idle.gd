class_name Demo2PlayerMovementIdleState
extends State

@onready var physics_controller := (owner as Entity).find_controller(Demo2PhysicsCharacterController) as Demo2PhysicsCharacterController;


func enter(_msg := {}) -> void:
	physics_controller.velocity = Vector3.ZERO;
	physics_controller.speed = 0;


func physics_update(_delta: float) -> void:
	if physics_controller.move_direction.length() >= 0.1:
		state_machine.transition_to("Walking");
	
	if Input.is_action_just_pressed(InputAction.PLAYER_JUMP):
		state_machine.transition_to("Jumping");


func exit() -> void:
	pass
