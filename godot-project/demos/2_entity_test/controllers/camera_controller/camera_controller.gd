class_name Demo2CameraController
extends Camera3D

var pitch_input: float;
var yaw_input: float;


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);

