class_name Demo2AnimationController
extends Node

signal animation_finished();

@export var animation_player: AnimationPlayer;


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
	animation_player.animation_finished.connect(_on_animation_finished);


func _on_animation_finished(_anim_name: StringName) -> void:
	animation_finished.emit();


func play(animation_name: StringName, custom_blend: float = -1, speed: float = 1) -> void:
	animation_player.play(animation_name, custom_blend, speed);

