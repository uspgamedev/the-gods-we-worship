extends Node3D

@export var player: Entity;


func _ready() -> void:
	var player_physics_controller := player.find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;
	player_physics_controller.position.y = 10;
