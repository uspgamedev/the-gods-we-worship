class_name Demo1PlayerPhysicsController
extends CharacterBody3D

@export var walk_speed: float = 2.0;
@export var run_speed: float = 7.0;
@export var rolling_speed: float = 6.0;
@export var jump_strength: float = 7.0;
@export var gravity: float = 50.0;
@export var rotation_speed: float = 10;

var speed: float;
var move_direction: Vector3;
var look_direction: Vector2;
var running: bool = false;
var jumping: bool = false;
var just_landed: bool = false;
var just_jumped: bool = false;


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
