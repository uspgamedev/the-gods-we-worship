class_name Demo1PlayerSpringArmController
extends SpringArm3D


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
