class_name Demo1PlayerAnimationController
extends Node

signal animation_finished();

@export var animation_player: AnimationPlayer;


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
	
	animation_player.animation_finished.connect(_on_animation_finished);


func _on_animation_finished(_anim_name: StringName) -> void:
	animation_finished.emit();


func play(animation_name: StringName, custom_blend: float = -1, speed: float = 1) -> void:
	animation_player.play(animation_name, custom_blend, speed);


func play_idle(custom_blend: float = -1, speed: float = 1.0) -> void:
	animation_player.play("idle", custom_blend, speed);


func play_walking(custom_blend: float = -1, speed: float = 1.0) -> void:
	animation_player.play("walking", custom_blend, speed);


func play_running(custom_blend: float = -1, speed: float = 1.0) -> void:
	animation_player.play("running", custom_blend, speed);


func play_jumping(custom_blend: float = -1, speed: float = 1.0) -> void:
	animation_player.play("jumping", custom_blend, speed);


func play_rolling(custom_blend: float = -1, speed: float = 1.0) -> void:
	animation_player.play("rolling", custom_blend, speed);
