class_name Demo1PlayerVisualController
extends Node3D


func _ready() -> void:
	self.add_to_group(Groups.CONTROLLER);
	(owner as Entity).register_controller(self);
