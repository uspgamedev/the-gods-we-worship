class_name Demo1PlayerCameraStateMachine
extends StateMachine

@export var spring_arm_controller: SpringArm3D;
@export var spring_arm_pivot: Node3D;
@export var camera_controller: Camera3D;
@export var camera_target: Node3D;

func initialize() -> void:
	$Free.spring_arm_controller = spring_arm_controller;
	$Free.spring_arm_pivot = spring_arm_pivot;
	$Free.camera_controller = camera_controller;
	$Free.camera_target = camera_target;
