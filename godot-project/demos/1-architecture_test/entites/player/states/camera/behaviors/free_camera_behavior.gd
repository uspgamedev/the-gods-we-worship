class_name Demo1FreeCameraBehavior
extends Behavior

@export var spring_arm_pivot: Node3D;
@export var camera_target: Node3D;
@export var follow_speed: float = 20;
@export var camera_speed: float = 100;

@onready var spring_arm_controller := (owner as Entity).find_controller(Demo1PlayerSpringArmController) as Demo1PlayerSpringArmController;
@onready var camera_controller := (owner as Entity).find_controller(Demo1PlayerCameraController) as Demo1PlayerCameraController;


func physics_update(delta: float) -> void:
	var camera_pitch: float = Input.get_axis(InputAction.CAMERA_BACKWARD, InputAction.CAMERA_FORWARD);
	var camera_rotation: float = Input.get_axis(InputAction.CAMERA_LEFT, InputAction.CAMERA_RIGHT);
	
	spring_arm_controller.rotation_degrees.x += camera_pitch * camera_speed * delta;
	spring_arm_controller.rotation_degrees.x = clampf(spring_arm_controller.rotation_degrees.x, -50.0, 30.0);
	
	spring_arm_controller.rotation_degrees.y -= camera_rotation *  camera_speed * delta;
	spring_arm_controller.rotation_degrees.y = wrapf(spring_arm_controller.rotation_degrees.y, 0, 360);
	
	camera_controller.position = lerp(camera_controller.position, camera_target.global_position, follow_speed * delta);
	camera_controller.rotation.x = lerp_angle(camera_controller.rotation.x, camera_target.global_rotation.x, follow_speed * delta);
	camera_controller.rotation.y = lerp_angle(camera_controller.rotation.y, camera_target.global_rotation.y, follow_speed * delta);
	camera_controller.rotation.z = lerp_angle(camera_controller.rotation.z, camera_target.global_rotation.z, follow_speed * delta);

