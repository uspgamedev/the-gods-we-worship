class_name Demo1PlayerJumpingState
extends State

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func enter(_msg := {}) -> void:
	physics_controller.just_jumped = true;


func physics_update(_delta: float) -> void:
	if physics_controller.just_landed:
		state_machine.transition_to("Idle");


func exit() -> void:
	physics_controller.just_landed = false;
