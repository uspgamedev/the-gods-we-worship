class_name Demo1PlayerRunningState
extends State

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func enter(_msg := {}) -> void:
	pass


func physics_update(_delta: float) -> void:
	if not Input.is_action_pressed(InputAction.PLAYER_RUN):
		state_machine.transition_to("Walking");
	
	if physics_controller.move_direction.length() <= 0.1:
		state_machine.transition_to("Idle");
	
	if Input.is_action_just_pressed(InputAction.PLAYER_JUMP):
		state_machine.transition_to("Jumping");


func exit() -> void:
	pass
