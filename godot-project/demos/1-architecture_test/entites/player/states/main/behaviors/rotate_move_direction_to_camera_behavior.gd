class_name Demo1RotateMoveDirectionToCameraBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;
@onready var spring_arm_controller := (owner as Entity).find_controller(Demo1PlayerSpringArmController) as Demo1PlayerSpringArmController;


func physics_update(_delta: float) -> void:
	physics_controller.move_direction = physics_controller.move_direction.rotated(Vector3.UP, spring_arm_controller.rotation.y);
