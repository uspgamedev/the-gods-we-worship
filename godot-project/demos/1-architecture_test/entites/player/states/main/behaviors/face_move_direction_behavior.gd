class_name Demo1FaceMoveDirectionBehavior
extends Behavior


@onready var visual_controller := (owner as Entity).find_controller(Demo1PlayerVisualController) as Demo1PlayerVisualController;
@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func physics_update(delta: float) -> void:
	physics_controller.look_direction = Vector2(physics_controller.velocity.z, physics_controller.velocity.x);
	visual_controller.rotation.y = lerp_angle(
		visual_controller.rotation.y, 
		physics_controller.look_direction.angle(), 
		physics_controller.rotation_speed * delta
	);
