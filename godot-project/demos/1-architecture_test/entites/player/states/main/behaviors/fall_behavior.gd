class_name Demo1FallBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func physics_update(delta: float) -> void:
	physics_controller.velocity.y -= physics_controller.gravity * delta;
