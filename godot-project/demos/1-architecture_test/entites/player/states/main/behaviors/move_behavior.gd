class_name Demo1MoveBehavior
extends Behavior

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func physics_update(_delta: float) -> void:
	physics_controller.move_and_slide();
