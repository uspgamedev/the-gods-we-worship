class_name Demo1PlayerWalkingState
extends State

@export var run_button_hold_timer: Timer;

@onready var physics_controller := (owner as Entity).find_controller(Demo1PlayerPhysicsController) as Demo1PlayerPhysicsController;


func _ready() -> void:
	run_button_hold_timer.timeout.connect(on_run_button_hold_timer_timeout);


func enter(_msg := {}) -> void:
	pass


func physics_update(_delta: float) -> void:
	if Input.is_action_pressed(InputAction.PLAYER_RUN) and run_button_hold_timer.is_stopped():
		run_button_hold_timer.start();
	elif Input.is_action_just_released(InputAction.PLAYER_RUN):
		state_machine.transition_to("Rolling");
		run_button_hold_timer.stop();
	
	if physics_controller.move_direction.length() <= 0.1:
		state_machine.transition_to("Idle");
	
	if Input.is_action_just_pressed(InputAction.PLAYER_JUMP):
		state_machine.transition_to("Jumping");
	
	$PlayAnimationBehavior.custom_speed = physics_controller.move_direction.length();


func exit() -> void:
	run_button_hold_timer.stop();


func on_run_button_hold_timer_timeout() -> void:
	state_machine.transition_to("Running");
